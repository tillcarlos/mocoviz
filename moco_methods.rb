
def fetch_time_logs(api_key, from_date, to_date)
  headers = {
    "Authorization" => "Token token=#{api_key}",
    "Content-Type" => "application/json"
  }

  params = {
    from: from_date.to_s,
    to: to_date.to_s
  }

  base_url = ENV['MOCO_DOMAIN'] || "api"
  url = "https://#{base_url}.mocoapp.com/api/v1/activities"
  response = HTTParty.get(url, headers: headers, query: params)
  JSON.parse(response.body)
end

def display_time_logs(time_logs, start_date, end_date)
  puts "Time logs from #{start_date} to #{end_date}:"
  if time_logs.empty?
    puts "No time logs found."
  else
    time_logs.each do |log|
        tag = "##{log['tag']}" if log['tag']
        txt = "#{log['description']} #{tag}"
      puts "Date: #{log['date']}, Hours: #{log['hours']}, Description: #{txt}"
    end
  end
  puts ""
end

# Get the date range for the current and last month
def date_range
  today = Date.today
  current_month_start = Date.new(today.year, today.month, 1)
  last_month_end = current_month_start.prev_day
  last_month_start = Date.new(last_month_end.year, last_month_end.month, 1)

  [last_month_start, last_month_end, current_month_start, today]
end


def display_logs(users)
  last_month_start, last_month_end, current_month_start, current_month_end = date_range

  users['users'].each_with_index do |user_data, index|
    api_key_value = ENV[user_data['api_key']]
    puts "__________________________________"
    puts "Team member #{user_data['name']}"
    puts "Last month:"
    last_month_time_logs = fetch_time_logs(api_key_value, last_month_start, last_month_end)
    display_time_logs(last_month_time_logs, last_month_start, last_month_end)

    puts "Current month:"
    current_month_time_logs = fetch_time_logs(api_key_value, current_month_start, current_month_end)
    display_time_logs(current_month_time_logs, current_month_start, current_month_end)

    export_to_csv(last_month_time_logs + current_month_time_logs, "#{user_data['name']}_time_logs.csv")
    puts "Exported to CSV: #{user_data['name']}_time_logs.csv"
  end
end

