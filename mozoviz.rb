require 'httparty'
require 'json'
require 'date'
require 'awesome_print'
require 'dotenv'
require 'yaml'
require './moco_methods.rb'

Dotenv.load




def export_to_csv(time_logs, file_name)
  CSV.open(file_name, 'wb') do |csv|
    csv << ['Date', 'Hours', 'Description', 'Tag']
    time_logs.each do |log|
      tag = log['tag'] || ''
      description = log['description'] || ''
      csv << [log['date'], log['hours'], description, tag]
    end
  end
end


# Load API keys from the YAML file
users = YAML.load_file('users.yml')

# this is not used right now
# display_logs(users['users'])

start_of_year = Date.new(Date.today.year, 1, 1)

date = Date.today.to_s.gsub(' ', '_')
csv_filename = "exports/export_all_users_#{date}.csv"
CSV.open(csv_filename, 'wb') do |csv|
  csv << ['User', 'Date', 'Hours', 'Description', 'Tag']

  users['users'].each_with_index do |user_data, index|
    api_key_value = ENV[user_data['api_key']]
    puts "Team member #{user_data['name']}"
    
    logs = fetch_time_logs(api_key_value, start_of_year, Date.today)
    
    logs.each do |log|
      tag = "##{log['tag']}" if log['tag']
      txt = "#{log['description']} #{tag}"
      # puts "Date: #{log['date']}, Hours: #{log['hours']}, Description: #{txt}"
      csv << [user_data['name'], log['date'], log['hours'], txt, tag]
    end
  end

end



